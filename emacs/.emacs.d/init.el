(setq inhibit-startup-message t)
(setq create-lockfiles nil)

;; Disable tool bar, menu bar, scroll bar.
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(global-display-line-numbers-mode 1)
(setq display-line-numbers-type 'relative)
(setq fill-column 100)
(setq warning-minimum-level :emergency)
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)

;; Highlight current line.
(global-hl-line-mode t)

;; Set Dvorak keyboard
(require 'quail)

(add-to-list 'quail-keyboard-layout-alist
             `("dvorak" . ,(concat "                              "
                                   "  1!2@3#4$5%6^7&8*9(0)[{]}`~  "
                                   "  '\",<.>pPyYfFgGcCrRlL/?=+    "
                                   "  aAoOeEuUiIdDhHtTnNsS-_\\|    "
                                   "  ;:qQjJkKxXbBmMwWvVzZ        "
                                   "                              ")))

(quail-set-keyboard-layout "dvorak")

;; Melpa setup
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(gruvbox))
 '(custom-safe-themes
   '("d445c7b530713eac282ecdeea07a8fa59692c83045bf84dd112dd738c7bcad1d" "ba323a013c25b355eb9a0550541573d535831c557674c8d59b9ac6aa720c21d3" "944d52450c57b7cbba08f9b3d08095eb7a5541b0ecfb3a0a9ecd4a18f3c28948" "f25f174e4e3dbccfcb468b8123454b3c61ba94a7ae0a870905141b050ad94b8f" "046a2b81d13afddae309930ef85d458c4f5d278a69448e5a5261a5c78598e012" "a5270d86fac30303c5910be7403467662d7601b821af2ff0c4eb181153ebfc0a" "871b064b53235facde040f6bdfa28d03d9f4b966d8ce28fb1725313731a2bcc8" "7b8f5bbdc7c316ee62f271acf6bcd0e0b8a272fdffe908f8c920b0ba34871d98" default))
 '(ispell-dictionary nil)
 '(package-selected-packages
   '(dracula-theme lsp-ui company lsp-java dart-server 2048-game git wrap-region org-roam projectile all-the-icons-completion all-the-icons-ibuffer all-the-icons exwm-surf exwm ## alsamixer flutter-l10n-flycheck hlint-refactor docker pdf-tools restart-emacs haskell-emacs elixir-mode json-mode js2-refactor js2-mode multiple-cursors evil-surround evil-easymotion evil-collection magit dashboard evil pass lsp-pyright flutter gruvbox-theme lsp-dart lsp-haskell cmake-mode org minimap)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Forge package to interact with Github
(use-package forge
  :after magit)
(setq auth-sources '("~/.authinfo"))


;; Import $PATH variable to eshell
(require 'exec-path-from-shell)
(exec-path-from-shell-initialize)

;; Treesit configs
(require 'treesit)
(setq treesit-language-source-alist
      '((cpp "https://github.com/tree-sitter/tree-sitter-cpp")
        (c "https://github.com/tree-sitter/tree-sitter-c")
	(elisp "https://github.com/Wilfred/tree-sitter-elisp/tree/e5524fdccf8c22fc726474a910e4ade976dfc7bb")
	))

;; Download Evil
(unless (package-installed-p 'evil)
  (package-install 'evil))

;; Evil custom variables
(setq evil-want-keybinding t)
(setq evil-want-C-u-scroll t)
(setq evil-want-C-d-scroll t)
(setq evil-want-minibuffer t)

;; Enable Evil
(require 'evil)
(evil-mode 1)

;; Evil configs
(evil-collection-init)
(evil-set-undo-system 'undo-redo)

;; Evil surround package
(require 'evil-surround)
(global-evil-surround-mode 1)


;; Evil text objecst
(require 'evil-textobj-tree-sitter)
;; bind `function.outer`(entire function block) to `f` for use in things like `vaf`, `yaf`
(define-key evil-outer-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.outer"))
;; bind `function.inner`(function block without name and args) to `f` for use in things like `vif`, `yif`
(define-key evil-inner-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.inner"))

(define-key evil-inner-text-objects-map "l" (evil-textobj-tree-sitter-get-textobj "loop.inner"))
(define-key evil-outer-text-objects-map "l" (evil-textobj-tree-sitter-get-textobj "loop.outer"))

(define-key evil-inner-text-objects-map "c" (evil-textobj-tree-sitter-get-textobj "class.inner"))
(define-key evil-outer-text-objects-map "c" (evil-textobj-tree-sitter-get-textobj "class.outer"))

(define-key evil-inner-text-objects-map "p" (evil-textobj-tree-sitter-get-textobj "parameter.inner"))
(define-key evil-outer-text-objects-map "p" (evil-textobj-tree-sitter-get-textobj "parameter.outer"))

(define-key evil-inner-text-objects-map "/" (evil-textobj-tree-sitter-get-textobj "comment.inner"))
(define-key evil-outer-text-objects-map "/" (evil-textobj-tree-sitter-get-textobj "comment.outer"))

;; Evil folding package
(require 'evil-vimish-fold)
(setq evil-vimish-fold-target-modes '(prog-mode conf-mode text-mode))
(global-evil-vimish-fold-mode 1)

;; Keybindings
(keymap-global-set "C-x p i" 'package-install)
(keymap-global-set "C-SPC" 'find-file)
(keymap-global-set "C-x g" 'magit)
(keymap-global-set "C-x t" 'treemacs)
(keymap-global-set "C-x c" 'company-complete)
(global-set-key (kbd "C-v") 'yank)

;; Parens system
(require 'smartparens-config)
(smartparens-global-mode)

;; Initialization dashboard
(require 'dashboard)
(dashboard-setup-startup-hook)
(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
(setq dashboard-banner-logo-title "ScantelMacs")
(setq dashboard-startup-banner 'logo)
(setq dashboard-center-text t)
(setq dashboard-show-shortcuts t)
(setq dashboard-icon-type 'all-the-icons)
(setq dashboard-items '((recents  . 5)
                        (projects . 5)
                        (bookmarks . 5)
                        (agenda . 5)
                        (registers . 5)))
;; Cheatsheet package
(require 'which-key)
(which-key-mode)

;; Powerline package
(require 'powerline)
(powerline-default-theme)

;; Powerline evil integration package
(require 'powerline-evil)

;; Multiple cursors package
(require 'multiple-cursors)

;; Error check/highlight package
(require 'flycheck)
(global-flycheck-mode)

;; Completion package configs
(add-hook 'after-init-hook 'global-company-mode)
(setq company-minimum-prefix-length 1)

;; LSP configs
(setq lsp-keymap-prefix "C-x l")
(require 'lsp-mode)
(add-hook 'prog-mode-hook #'lsp)

;; Dart + Flutter configs
(with-eval-after-load 'lsp-mode

(require 'lsp-dart)
(setq lsp-dart-sdk-dir "~/flutter/bin/cache/dart-sdk"
    lsp-dart-flutter-sdk-dir "~/flutter"
    flutter-sdk-path "~/flutter"
    lsp-dart-line-length 100
    lsp-dart-show-todos t
    )

(setq lsp-dart-test-extra-args '("--no-sound-null-safety"))


;; Enabling DAP configs
(setq dap-auto-configure-features '(sessions locals controls tooltip))


;; Personal information
(setq user-full-name "Luis Scantelbury"
(setq user-mail-address "l.scantelbury@gmail.com")

;; Dotfiles Stow path
(setq dotfiles-path "~/.dotfiles/emacs/.emacs.d/init.el")

;; Find Stow/Standard path
(defun find-init-file ()
  "Return the init.el standard location if dotfiles is not found."
  (if (file-exists-p dotfiles-path)
      dotfiles-path
    "~/.emacs.d/init.el"))

;; Set init file variable to Stow/Standard path
(setq user-init-file (find-init-file))

;; Function to edit file
(defun edit-init-file () "Edit init file" (interactive) (find-file user-init-file))
(keymap-global-set "C-x RET e" 'edit-init-file)
